<?php

/**
 * Extension Manager/Repository config file for ext "jbspb_bootswatch".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'JBSPB Bootswatch',
    'description' => 'Sitepackagebuilder für Bootswatch Beispiel in rst-typo3',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '10.2.0-11.5.99',
            'fluid_styled_content' => '10.2.0-11.5.99',
            'rte_ckeditor' => '10.2.0-11.5.99',
            'indexed_search' => '10.2.0-11.5.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Becss\\JbspbBootswatch\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Joe Brandes',
    'author_email' => 'joe.brandes@example.com',
    'author_company' => 'BECSS',
    'version' => '11.5.99',
];
